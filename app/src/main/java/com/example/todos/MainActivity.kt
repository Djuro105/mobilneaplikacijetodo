package com.example.todos

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.SearchView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.note.view.*
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    var listNotes = ArrayList<Note>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sendEmailBtn.setOnClickListener(){
            val Recipient = "tjansky@vub.hr"
            val Subject = "mobilne"
            val Message = "Uspio poslati mail preko mobilne aplikacije"

            sendEmail(Recipient, Subject, Message)
        }


        LoadQuery("%")

    }

    fun sendEmail(Recipient:String, Subject:String, Message:String){
        val emailIntend = Intent(Intent.ACTION_SEND)
        emailIntend.data = Uri.parse("mailto")
        emailIntend.type = "text/plain"

        emailIntend.putExtra(Intent.EXTRA_EMAIL, arrayOf(Recipient))
        emailIntend.putExtra(Intent.EXTRA_SUBJECT, Subject)
        emailIntend.putExtra(Intent.EXTRA_TEXT, Message)

        try {
            startActivity(Intent.createChooser(emailIntend,"Odaberi mail"))
        }
        catch(e:Exception) {
            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
        }


    }

    fun LoadQuery(title: String){
        var dbManager = DbManager(this)

        val selectionArgs = arrayOf(title)
        val projections = arrayOf("ID", "Title", "Description")
        val cursor = dbManager.Query(projections, "Title like ?", selectionArgs, "Title")
        listNotes.clear()

        println("logiranje " + "cccccccc")

        if(cursor.moveToFirst()){
            do{
                val ID = cursor.getInt(cursor.getColumnIndex("ID"))
                val Description = cursor.getString(cursor.getColumnIndex("Description"))
                val Title = cursor.getString(cursor.getColumnIndex("title"))


                listNotes.add(Note(ID,Title,Description))

            } while(cursor.moveToNext())
        }

        var myNotesAdapter = MyNotesAdapter(this, listNotes)
        lvNotes.adapter = myNotesAdapter

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)

        val sv = menu?.findItem(R.id.app_bar_search)?.actionView as SearchView
        val sm = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        sv.setSearchableInfo(sm.getSearchableInfo(componentName))
        sv.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                //Toast.makeText(applicationContext, query, Toast.LENGTH_LONG).show()
                //LoadQuery("%" + query + "%")
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if(newText == ""){
                    LoadQuery("%")
                } else {
                    //Toast.makeText(applicationContext, newText, Toast.LENGTH_LONG).show()
                    LoadQuery("%" + newText + "%")
                }

                return false
            }
        })
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) {
            when(item.itemId){
                R.id.addNote->{
                    var intent = Intent(this, AddNote::class.java)
                    startActivity(intent)
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }


    inner class MyNotesAdapter: BaseAdapter{
        var listNotesAdapter = ArrayList<Note>()
        var context: Context ?= null
        constructor(context:Context, listNotesAdapter: ArrayList<Note>):super(){
            this.listNotesAdapter = listNotesAdapter
            this.context = context
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
           var myView = layoutInflater.inflate(R.layout.note, null)
           var myNote = listNotesAdapter[position]
           myView.tvTitle.text = myNote.noteName
           myView.tvDes.text = myNote.noteContent
           myView.ivDelete.setOnClickListener(View.OnClickListener {
               var dbManager = DbManager(this.context!!)
               val selectionArgs = arrayOf(myNote.noteId.toString())
               dbManager.Delete("ID=?", selectionArgs)
               LoadQuery("%")
           })

            return myView
        }

        override fun getItem(position: Int): Any {
            return listNotesAdapter[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return listNotesAdapter.size
        }

    }


}
