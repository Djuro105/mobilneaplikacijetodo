package com.example.todos

class Note {

    var noteId: Int ?= null
    var noteName: String ?= null
    var noteContent: String ?= null

    constructor(noteId: Int, noteName: String, noteContent: String){
        this.noteId = noteId
        this.noteName = noteName
        this.noteContent = noteContent
    }
}